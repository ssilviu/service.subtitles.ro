import urllib, urllib2, re, zipfile, rarfile, os, time, shutil, subprocess, difflib
from bs4 import BeautifulSoup
import xbmc

def log(msg):
    #print msg
    module = '[SUB_RO]'
    xbmc.log((u"### [%s] - %s" % (module,msg,)).encode('utf-8'), level=xbmc.LOGNOTICE)

class Movie:
    def __init__(self, item):
        self.item = item

    def get_title(self):
        return re.sub('\[[^<]+?\]|\((.+)\)', '', self.item['title'])

    def is_fit(self, subtitle):
        if difflib.SequenceMatcher(None, self.item['title'], subtitle).ratio() < .25:
            return False
        return True

    def is_fit_subtitle(self, subtitle):
        return True


class Show:
    def __init__(self, item):
        self.item = item

    def get_title(self):
        return self.item['tvshow']

    def is_fit(self, subtitle):
        if difflib.SequenceMatcher(None, self.item['tvshow'], subtitle).ratio() < .66:
            return False
        if ' {} '.format(self.item['season']) not in subtitle:
            return False
        return True

    def is_fit_subtitle(self, subtitle):
        s = self.item['season']
        e = self.item['episode'].zfill(2)
        checks = [
            '{}e{}'.format(s, e),
            '{}{}'.format(s, e),
            '{}x{}'.format(s, e)
        ]
        for check in checks:
            if check in subtitle.lower():
                return True

        return False


class Titrari(object):
    url = 'https://www.titrari.ro'
    items_per_page = 20

    def __init__(self, temp_folder):
        self.temp_folder = temp_folder

    def is_unrar_available(self):
        try:
            subprocess.call(['which', 'unrar'])
        except:
            log('unrar is not installed')
            return False
        return True

    def urlCautareAvansata(self, title):
        data = {
            'page': 'cautare',
            'z1': '0',
            'z2': title,
            'z3': '1',
            'z4': '1'
        }
        return '{}/index.php?{}'.format(self.url, urllib.urlencode(data))

    def parserCautareAvansata(self, html):
        table = html.find_all('table', class_='forumline')[10]

        titles = table.find_all('h1')
        zip_subs = html.find_all('a', href=re.compile('^get.php\?id='))

        for i, zip_sub in enumerate(titles):
            subid = zip_subs[i].get('href').split('=')[-1]
            log('Found {} {}'.format(subid, zip_sub.get_text()))
            yield subid, zip_sub.get_text()

    def urlZipFile(self, id):
        return '{}/get.php?id={}'.format(self.url, id)

    def makeRequest(self, url, referrer = None):
        log('Get {}'.format(url))
        req = urllib2.Request(url)
        req.add_header('User-Agent', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.71 Safari/537.36')
        req.add_header('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8')
        if referrer:
            req.add_header('Referer', referrer)
        return urllib2.urlopen(req)

    def extractSubs(self, archive, subid_folder):
        allSubs = []

        if archive.endswith('.zip'):
            with zipfile.ZipFile(archive) as zip_file:
                for subtitle in self.extract(zip_file, subid_folder):
                    allSubs.append(subtitle)
        elif archive.endswith('.rar') and self.is_unrar_available():
            with rarfile.RarFile(archive) as rar_file:
                for subtitle in self.extract(rar_file, subid_folder):
                    allSubs.append(subtitle)

        return allSubs

    def extract(self, zip_file, subid_folder):
        for member in zip_file.namelist():
            filename = os.path.basename(member)

            # skip directories
            if not filename:
                continue

            filename = re.sub('[^\w\s.-]', '', filename)

            try:
                source = zip_file.open(member)
                target = file(os.path.join(subid_folder, filename), "wb")

                with source, target:
                    shutil.copyfileobj(source, target)

                yield filename

            except Exception, e:
                log('cannot extract : {}'.format(e))

    def handleSubsArchive(self, subid, url_listing):
        subid_folder = os.path.join(self.temp_folder, 'titrari', subid)

        # check if cached
        if os.path.isdir(subid_folder):
            log('Found in cache')
            return os.listdir(subid_folder)

        try:
            os.makedirs(subid_folder)
        except Exception, e:
            log('cannot create folder {} : {}'.format(subid_folder, e))

        # download arhiva subtitrari
        url_zip = self.urlZipFile(subid)
        resp = self.makeRequest(url_zip, url_listing)
        filename = dict(resp.info())['content-disposition'].split('"')[-2]

        # save subtile archive
        archive = os.path.join(subid_folder, filename)
        with open(archive, 'wb') as code:
            code.write(resp.read())

        subs = self.extractSubs(archive, subid_folder)
        os.remove(archive)
        return subs

    def search(self, item):
        log(item)

        if 'tvshow' in item.keys() and item['tvshow']:
            video = Show(item)
        else:
            video = Movie(item)

        # cautare avansata
        url_listing = self.urlCautareAvansata(video.get_title())
        resp = self.makeRequest(url_listing)
        html = BeautifulSoup(resp.read(), 'html.parser')

        for subid, title in self.parserCautareAvansata(html):
            subtitles = self.handleSubsArchive(subid, url_listing)

            # check for is fit subtitle
            for subtitle in subtitles:
                if video.is_fit_subtitle(subtitle):
                    yield 'titrari', subid, subtitle


# for dev & testing
if __name__ == '__main__':
    item = {'title' : 'Survivalist', 'year' : '2015'}
    item = {'tvshow': 'The Big Bang Theory', 'season': '8', 'episode': '8'}

    service = Titrari('f')
    for sub in service.search(item):
        print sub
