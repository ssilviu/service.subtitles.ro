# service.subtitles.ro - OSMC subtitle plugin #

# INSTALL - raspberry pi 2 #

* sudo apt-get install git-core
* sudo apt-get install unrar
* sudo apt-get install python-pip
* sudo pip install beautifulsoup4
* sudo pip install rarfile
* cd /home/osmc/.kodi/addons
* git clone https://ssilviu@bitbucket.org/ssilviu/service.subtitles.ro.git